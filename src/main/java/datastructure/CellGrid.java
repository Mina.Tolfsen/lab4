package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][]grid;

    public CellGrid(int rows, int columns, CellState initialState) {

        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];
		
        for(int i = 0; i < rows; i++){
            for(int n = 0; n < columns; n++){
                grid[i][n] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(numRows() < row && row < 0){
            throw new IndexOutOfBoundsException();
        }
        if(numColumns() < column && column < 0){
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        if(numRows() < row && row < 0 ){
            throw new IndexOutOfBoundsException();
        }
        if(numColumns() < column && column < 0) {
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(this.rows, this.columns, CellState.DEAD);

        for(int i = 0; i < numRows(); i++){
            for(int n = 0; n < numColumns(); n++){
                gridCopy.set(i, n, grid[i][n]);
            }
        }
        return gridCopy;
    }
    
}
